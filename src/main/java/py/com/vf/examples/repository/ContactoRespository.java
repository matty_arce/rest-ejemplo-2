package py.com.vf.examples.repository;

import java.util.List;

import py.com.vf.examples.bean.Contacto;

public interface ContactoRespository {

	List<Contacto> obtenerContactos();
	Contacto agregarContacto(Contacto contacto);
	boolean eliminarContacto(Integer id);
	boolean actualizarContacto(Contacto contacto, Integer id);
	Contacto obtenerContacto(Integer id);
}
